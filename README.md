# ChirpCoupons
Chirp (https://chirp.io/) has been used to transfer data over inaudible sound.

1. Use Chirp's command line tool to encode a coupon code into inaudible sound file.
   $ chirp-audio-write -u "<COUPON_CODE>" <filename>.wav 

2. Overlay this file into any audio file (.wav) (maybe using Audacity).

3. Install the android app in mobile phone.

4. Open the app and click on 'Start Listening' button.

5. Play the overlayed audio using any media player (say VLC, iTunes etc).

6. The coupon code will be displayed in the screen. Save it for future use during offline shopping.

![picture](screenshots/step1.png)
![picture](screenshots/step2.png)

![picture](screenshots/step3.png)
![picture](screenshots/step4.png)
