package io.chirp.chirpwifi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import io.chirp.connect.ChirpConnect;
import io.chirp.connect.interfaces.ConnectEventListener;
import io.chirp.connect.interfaces.ConnectSetConfigListener;
import io.chirp.connect.models.ChirpError;
import io.chirp.connect.models.ConnectState;

public class MainActivity extends AppCompatActivity {

    ChirpConnect chirpConnect;
    Context context;

    TextView status;
    TextView startStopListeningBtn;
    Button saveButton;

    ArrayList<String> savedCoupons = new ArrayList<String>();
    String currentCoupon;

    final int RESULT_REQUEST_PERMISSIONS = 11;

    final int PERMISSION_REQUEST_CODE_WRITE_CONTACTS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status = findViewById(R.id.status);
        startStopListeningBtn = findViewById(R.id.startStopListening);
        context = this;
        startStopListeningBtn.setAlpha(.4f);
        startStopListeningBtn.setClickable(false);

        saveButton = findViewById(R.id.saveCoupon);
        saveButton.setVisibility(View.INVISIBLE);
        /*
        You can download config string and credentials from your admin panel at developers.chirp.io
         */
        String KEY = "F5b16beF58eB9A032bEeafBdA";
        String SECRET = "a30174b092255bA80edfB7a736dBF4e0833D3DC06BBAb4fD81";
        String CONFIG = "Lq0Y5zBUDnlvqLM6LLlGnDe/BynKX23QbJJi5xQTkOySb5khpTSdVRpJDsHmzBwdZ8GDKhRpCK+xyCOKMECLq44BJLJW53l1CFtYsEwGWHAQ7k3Y3VY8SdBFKBiaiG4/KF/91SLe3ZB6Tv+Kbu2aC/5s5+4msHh1uB2JQAfKLYWjbEtcldTU926j3K3fsHUqnYcKTDhK8ND6KLu5kjbQ4IxQ+12WaoJFX2Q/BfdHONKL5Dnfx8Bz6xH7D7ks/OpOe0fYIMbvZJaN3ewfVlvwH1TvVtUEo7dTXAtKYzGgcedLv5PspL48b4XqIR4Nts8YyMFg+d/+1sK1+27i5nA/cElR88fuzlwxldzpiaZsW4HfTFhTk2r+Dd1/50fb/rW9cg/ZzhRoZ+4+BomgiQHnzTk42TvaXROiMKsXsDC3zXhUmah4VIwwP6oa80mC1wFKeag0pYy+EYdaGsHJBTEzN6PdWLiwO0vNRzosma8gRRyhpwIMJrNp1yb9kUD1pV5WCCU13p+TxqxKUldEz+IaztJBNKvBMVNhxDjmPTQFZaWInjx8eJc+5sC2yobP/SsRQ/VM/a3tN1ts56BJgjNdY65hP/pweH5GkIjAE8xHY24sw83kLH9QDxkuC5U1u3sj+pIppdAO5ZcnbbXFYMSXWr+5sVSzx/wtT+4nGClDjyzfHIb87dCQcFFPvgwRYZJNY3kqIPuQ0VS5itD87EImngBWQhsOJ73k8qt9kugmuANAB8VEhnOJmiqL5/GeD4R6HlUlSFVXJSwWywIy/QUbBWSwTbJFJduz/gXDjshAaQvwX4EfzPi9gPunNiXk3A9vjK5x33LudkJsSKdCqjZo1GynsDzUKKtcgiZWZ/1RRBTKQzfiOmrOGP9N5J05lflG3624yEtpk/URU7Fiev6U6FZ7XhRJZBkIVfFUD/3gySEhjY2YW24gLcS4nGMX9c7P9pXcbpApWHc/wwn4uqkR7u3cFOS3+I/tNVVXkB7Rmd+ha122WaQDbseSEa1I1W3GCyZZAuVhryWoOvpvUiTYkr4ANVjTnKoTXCFptl8hpT0ncQs/DEdWX99pXRtUCyGb2qtQY2Zv4+qv9iwvhwJcoOIww8sAL0MVjBSb3A8elaxBq/YZF1S7YWQGPPGK8ValCjiSZgGckhXd9Tiov6slJ47qc4sIBszKl74SGHqyh0/Q7IyjcxlydGZsL1E84K2UIetpJNqYs/o5fhjuqdsenagM9PUuquYU6enD4ouYx1yog0aZ+LqjT4psfXhNgn8CxR5RmJaDxXp2dFgNgiqVgrNyAUCQtI4DOE2ICaEBMuDR8yF8gz9lOE0EdE0d97Z5rob39RymESeJy6a+MjvtS4Enhlswanm5KQFqHW337K98s/yd1qIGp3w1Z1O8YxiCFKlatP+BlIug2m0gG4fCpi9Cz/Cuhpmi4VLyHcp7WEqakAWqzc9Ts20HeAhGzrmoIMkAYDBG5oc4m8z/W5j4e/J08EXgSOvntXAFzNsn8G+m27v2vArWgDTZ1rECLx09nmiAoqeNwy9G2WkQWo12mtbho2mWallIky2VCPFYKAmjRhxMesWM61IpMi5kubHLVrGGfo6Zc5LrqghFE/sHGb2g+T9TOg7cXyX5M1ziQfRdPg2pgDp0LJK/YZM65Nq4psAI/goajvo7g10vaY+Dg9IR7LKU1CUJYJA3CJg+uvyX1EvYfht9Ccsh3aFq+pAgM5yyRyImEfz5hMNpH8HaXayu34/ctXELrB7YxTl/9QqRw1e9XHdeexMJaTvkyBGzTgKNuxnN/ZtuW5Vg3rZRZamaWZMediqX5Q9DrcnGZKGVE4KttuLDDFUNtijnM9KWJpFvpIWkcIpYPazVg9FNKRrh+K/4aqcwBNoEjWVbN/YNx4wmUYDEMV2UDMGAjVTl5FKo2K1jz99sZ/FtYrVHmRBG4g+QUYfmDqUwN7JW/MD5F9XKEpOCAIzC9CgUjwwd2ikfeD6aHnT2xqBxDU2388vA4XCC99Ijuv3CwZ40UqNnEw1t+FljFDYzfOe7n0G6sFkNR1KXXkhLAK6ZZCiEfT7EHtqPQ3pxczwx+4vjWgg8asztPYXpIPWiFH/HzB2Bgd/KpQuZex+xQ2nhC27xr0uablwQux1qAst0QqR5GzhOYzW3JcVrAHpBHLTIYYm1XDGa5QCRj4VonMBLUffE/H1RdEaE4dBcArsx3ye30Y6CdaNezYjvDuomISOb3sgLIAXyNyKVARFHMTkl2Y45yS8Le3CA77x4vE0o9LrTXnNcdAZyInXSIDBPyJCflPA2EQDXRPxeA17leZAHuWnD8xli6pWbYBG1SlsiS+7V3yC2RcmfL/cJ33o/b/8sf17JXlEHJRtO2S0kjl6NPaQBAIHdov8Wg6Jo0gRu6wPTfF7SQszEOU8I/6RO4vsGsaGdgQZ0lyKiEEI0UF4wFmJz1NTQI0loY00suqU66weJppd/cWsb/bXx8gQVKwOwrxS8dKqAhc2dhMg2/WTMyQ0hAYgYLT6COF50kO9F5p9SaePTLEevwnN2oAqUbuVYHCGywUsubI7slLqgbqgy6lMZNttCO6i3YeourIfGSzrpgSQ=";


        chirpConnect = new ChirpConnect(this, KEY, SECRET);
        chirpConnect.setConfig(CONFIG, connectSetConfigListener);
        chirpConnect.setListener(connectEventListener);

    }

    ConnectSetConfigListener connectSetConfigListener = new ConnectSetConfigListener() {
        @Override
        public void onSuccess() {
            //The config is successfully set, we can enable Start/Stop button now
            startStopListeningBtn.setAlpha(1f);
            startStopListeningBtn.setClickable(true);
        }

        @Override
        public void onError(ChirpError setConfigError) {
            Log.e("SetConfigError", setConfigError.getMessage());
            setStatus("SetConfigError\n" + setConfigError.getMessage());
        }
    };

    ConnectEventListener connectEventListener = new ConnectEventListener() {

        @Override
        public void onSending(byte[] payload, byte channel) {
            /**
             * onSending is called when a send event begins.
             * The data argument contains the payload being sent.
             */
            String hexData = "null";
            if (payload != null) {
                hexData = chirpConnect.payloadToHexString(payload);
            }
            Log.v("connectdemoapp", "ConnectCallback: onSending: " + hexData + " on channel: " + channel);
        }

        @Override
        public void onSent(byte[] payload, byte channel) {
            /**
             * onSent is called when a send event has completed.
             * The data argument contains the payload that was sent.
             */
            String hexData = "null";
            if (payload != null) {
                hexData = chirpConnect.payloadToHexString(payload);
            }
            Log.v("connectdemoapp", "ConnectCallback: onSent: " + hexData + " on channel: " + channel);
        }

        @Override
        public void onReceiving(byte channel) {
            /**
             * onReceiving is called when a receive event begins.
             * No data has yet been received.
             */
            Log.v("connectdemoapp", "ConnectCallback: onReceiving on channel: " + channel);
            setStatus("Receiving...");
        }

        @Override
        public void onReceived(byte[] payload, byte channel) {
            /**
             * onReceived is called when a receive event has completed.
             * If the payload was decoded successfully, it is passed in data.
             * Otherwise, data is null.
             */
            String hexData = "null";
            if (payload != null) {
                hexData = chirpConnect.payloadToHexString(payload);
            }
            Log.v("connectdemoapp", "ConnectCallback: onReceived: " + hexData + " on channel: " + channel);

            try {
                currentCoupon = new String(payload, "UTF-8");
                Log.v("connectdemoapp", "Coupon: " + currentCoupon + "\n");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        status.setText(currentCoupon);
                        saveButton.setVisibility(View.VISIBLE);
                    }
                });
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                setStatus("Received data but unable to decode data...");
            }
        }

        @Override
        public void onStateChanged(byte oldState, byte newState) {
            /**
             * onStateChanged is called when the SDK changes state.
             */
            Log.v("connectdemoapp", "ConnectCallback: onStateChanged " + oldState + " -> " + newState);
            ConnectState state = ConnectState.createConnectState(newState);
            if (state == ConnectState.AudioStateRunning) {
                setStatus("Listening...");
            }

        }

        @Override
        public void onSystemVolumeChanged(int oldVolume, int newVolume) {
            Log.v("connectdemoapp", "System volume has been changed, notify user to increase the volume when sending data");
        }

    };

    public void getCoupons(View view) {
        Intent intent = new Intent(this, CouponsActivity.class);
        intent.putStringArrayListExtra("key",savedCoupons);
        startActivity(intent);
    }

    public void saveCoupon(View view) {
        savedCoupons.add(currentCoupon);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Coupon saved!!");
                saveButton.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void startStopListening(View view) {
        if (chirpConnect.getConnectState() == ConnectState.AudioStateStopped) {
            ChirpError startError = chirpConnect.start();
            if (startError.getCode() > 0) {
                Log.d("startStopListening", startError.getMessage());
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        status.setText("Listening...");
                        saveButton.setVisibility(View.INVISIBLE);
                    }
                });
                startStopListeningBtn.setText("Stop Listening");
            }

        } else {
            ChirpError stopError = chirpConnect.stop();
            if (stopError.getCode() > 0) {
                Log.d("startStopListening", stopError.getMessage());
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        status.setText("IDLE");
                        saveButton.setVisibility(View.INVISIBLE);
                    }
                });
                startStopListeningBtn.setText("Start Listening");
            }
        }
    }

    private void setStatus(final String newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText(newStatus);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, RESULT_REQUEST_PERMISSIONS);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS}, PERMISSION_REQUEST_CODE_WRITE_CONTACTS);
        }
        //saveButton.setVisibility(View.INVISIBLE);
    }
}
