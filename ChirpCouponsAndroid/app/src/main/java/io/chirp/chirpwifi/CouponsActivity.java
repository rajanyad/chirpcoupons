package io.chirp.chirpwifi;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CouponsActivity extends AppCompatActivity {

    private TextView myText;
    private ArrayList<String> coupons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);
        myText = (TextView) findViewById(R.id.tv);
        coupons = getIntent().getExtras().getStringArrayList("key");
        for (int i = 0; i < coupons.size(); i++) {
            myText.append(coupons.get(i));
            myText.append("\n");
            Log.v("coupons", coupons.get(i));
        }
    }
}


